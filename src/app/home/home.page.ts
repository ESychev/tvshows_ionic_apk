import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Channel } from '../models/data';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public channels: Channel[] = [];
  private loader: HTMLIonLoadingElement;
  constructor(private dataService: DataService,
    public loadingController: LoadingController){}

  async ngOnInit(){

  
    this.startLoadIndicate();
    this.dataService.getChannels().then(res => {
      console.log('load finished');
      this.channels = res;
      this.stopLoadIndicate();
      
    });
  }
  private async startLoadIndicate(){
    this.loader = await this.loadingController.create({
      message: 'загрузка'
    });
    this.loader.present();
  }

  private stopLoadIndicate(){
    setTimeout(() =>{
      this.loader.dismiss();
    },1000);
  }

}
