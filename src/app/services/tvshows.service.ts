import { Injectable } from "@angular/core";
import { Tvshow, LiveChannel } from '../models/data';
import * as moment from 'moment';
@Injectable()

export class TvshowsService{
    public tvshows: Tvshow[];

    getTvshowsByDate(date: string): Tvshow[]{
        return this.tvshows.filter(tvshow => tvshow.date === date);
    }

}
