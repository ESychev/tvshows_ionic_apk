import { Url } from 'url';

export interface Channel{
    

channel_id: number;

genres: number[];
logo: string;
name: string;
stream_url: string;


}

export interface Tvshow{
    channel_id: number;
    date: string;
    start: number;
    stop: number;
    title: string;
    ts: number;
    tvshow_id: string;
}

export interface LiveChannel{
    id: number;
    name: string;
    start: number;
    stop: number;
    logo: string;
    show_name: string;

}