import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import { Tvshow } from '../models/data';
import { TvshowsService } from '../services/tvshows.service';
import { LoadingController } from '@ionic/angular';

@Component({
    selector: 'dates',
    templateUrl: 'dates.page.html',
    styleUrls: ['dates.page.scss']
})

export class DatesPage implements OnInit{

    public uniqueDates: string[] = [];
    private loader: HTMLIonLoadingElement;
    constructor(private activatedRoute: ActivatedRoute, private dataService: DataService, private tvshowsService: TvshowsService,
        private loadingController: LoadingController){}

    ngOnInit(){
        this.startLoadIndicate();
        this.activatedRoute.queryParams.subscribe(params => {
            const id = params['id'];
            if(id){
                this.dataService.getTvShowsById(+id).then(res =>{
                    this.tvshowsService.tvshows=res;
                    this.uniqueDates = res.map(item => item.date).filter((date, index, array) =>{
                        return array.indexOf(date) === index;
                    });
                });
            }
        });
        this.stopLoadIndicate();
        
    }
    public get isHaveData(): boolean{
        return this.uniqueDates.length>0;
    }

    //private getUniqueDates(tvshows: Tvshow[]): any[]{}
    private async startLoadIndicate(){
        this.loader = await this.loadingController.create({
          message: 'загрузка'
        });
        this.loader.present();
      }
    
      private stopLoadIndicate(){
        setTimeout(() =>{
          this.loader.dismiss();
        },1000);
      }
}