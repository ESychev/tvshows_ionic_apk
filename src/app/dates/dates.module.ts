import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


import { DatesPage } from './dates.page';
import { ChannelComponent } from '../components/channel/channel.component';
import { DateComponent } from '../components/date/date.component';
import { DatePipe } from '../Pipes/date.pipe';
import { NotFoundModule } from '../components/notfound/notfound.module';
import { PipeModule } from '../Pipes/pipe.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotFoundModule,
    PipeModule,
    RouterModule.forChild([
      {
        path: '',
        component: DatesPage
      }
    ])
  ],
  declarations: [DatesPage,
    DateComponent,
    DatePipe
  
  ]
})
export class DatesPageModule {}