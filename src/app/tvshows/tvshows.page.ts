import { Component } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { TvshowsService } from '../services/tvshows.service';
import { Tvshow } from '../models/data';

@Component({
    selector: 'tvshows-page',
    templateUrl: 'tvshows.page.html',
    styleUrls: ['tvshows.page.scss']
})

export class TvshowsPage{
    public tvshows: Tvshow[] = [];
    private date: string;

    constructor(private activateRoute: ActivatedRoute,
        private tvshowsService: TvshowsService){}

    ngOnInit(){
        this.activateRoute.queryParams.subscribe(params => {
            if(params['date']){
                
                this.tvshows = this.tvshowsService.getTvshowsByDate(params['date']);
            }
        });
    }

}
