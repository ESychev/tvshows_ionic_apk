import { Component } from '@angular/core';

import { Platform, MenuController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menuCtrl: MenuController,
    private toastCtrl: ToastController
  ) 
  {    this.platform.ready().then(() => {
    this.statusBar.styleDefault();
    this.splashScreen.hide();

    let lastTimeBackPress = 0;
    const timePeriodToEdit = 2000;
    
    platform.backButton.subscribe(async()=>{
       if(new Date().getTime()- lastTimeBackPress){
         navigator['app'].exitApp();
       }else{
         let toast =await this.toastCtrl.create({
           message:' Для выхода из приложения назмите кнопку "Назад" ещё раз',
           duration: 3000,
           position: 'bottom'
         });
         toast.present();
         lastTimeBackPress = new Date().getTime();
       }
    });
    

  });

    
  }

  initializeApp() {

  }
  public closeMenu(): void{
    this.menuCtrl.close('main');
  }
}
