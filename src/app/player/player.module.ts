import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


import { PlayerPage } from './player.page';
import { TvshowComponent } from '../components/tvshow/tvshow.component';

import { NotFoundModule } from '../components/notfound/notfound.module';
import { PipeModule } from '../Pipes/pipe.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotFoundModule,
    PipeModule,
    RouterModule.forChild([
      {
        path: '',
        component: PlayerPage,
      }
    ])
  ],
  declarations: [PlayerPage,
  TvshowComponent


  ]
})
export class PlayerPageModule {}