import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import { resolve, reject } from 'q';


@Component({
    selector:'player-page',
    templateUrl: 'player.page.html',
    styleUrls:['player.page.scss']
})
export class PlayerPage implements OnInit{

    private channel_id: number;
    constructor(private activatedRoute: ActivatedRoute, private dataService: DataService){}

    ngOnInit(){
    this.activatedRoute.queryParams.subscribe(res=>{
        this.channel_id = +res['channel_id'];
        this.getStreamUrl().then(url=> {
            console.log('Stream url:',url);
        });
    });
} 

  private getStreamUrl(): Promise<string>{
      return new Promise(( resolve, reject)=>{
          this.dataService.getChannels().then(chs =>{
              const channel = chs.find(ch=> +ch.channel_id === this.channel_id);
        if (channel){
            resolve(channel.stream_url);
        }  else{
            reject('NO tream url');
        }        
            })
      })
  }


}