import {DatePipe} from './date.pipe';
import{TimmePipe} from './time.pipe';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

@NgModule({
    declarations:[
        DatePipe,
        TimmePipe
    ],
    imports:[
        CommonModule
    ],
    exports: [
        DatePipe,
        TimmePipe
    ]
})
export class PipeModule{}


