import { Component, OnInit, Input, AfterViewInit } from "@angular/core";
import *as moment from 'moment';
import { Router } from '@angular/router';

@Component({
    selector: 'date',
    templateUrl: 'date.component.html',
    styleUrls: ['date.component.scss']
})

export class  DateComponent implements OnInit, AfterViewInit{

    @Input() date: string;
    
    constructor(public router: Router){}

    ngOnInit(){

    }

    ngAfterViewInit(){
        if(this.isCurrent){
            setTimeout(()=>{
            const active = document.getElementsByClassName('date-string_active')[0];
            active.scrollIntoView({block: 'center', behavior: 'smooth'});
            },0);
        }
    }
    get isCurrent(): boolean{
        
        return moment(this.date).isSame(moment().format('YYYY-MM-DD'));
    }

    public showTvshows(){
        this.router.navigate(['/tvshows'], {queryParams: {date: this.date}});
    }
}