import { Component } from "@angular/core";
import { Router } from '@angular/router';

@Component({
    selector:'not-found',
    templateUrl: 'notfound.component.html',
    styleUrls:['notfound.component.scss']
})

export class NotFoundComponent{
    constructor(public router: Router){}

    public goBack(): void{
        this.router.navigate(['/home']);
    }

}