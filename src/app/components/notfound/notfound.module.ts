import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotFoundComponent} from './notfound.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [
        NotFoundComponent
    ],
    imports: [
        CommonModule,
        IonicModule
    ],
    exports: [
        NotFoundComponent
    ]
     
})

export class NotFoundModule{}