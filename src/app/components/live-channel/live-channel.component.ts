import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { Channel, Tvshow } from 'src/app/models/data';
import { DataService } from 'src/app/services/data.service';
import *as moment from 'moment'
@Component({
    selector:'live-channel',
    templateUrl:'live-channel.component.html',
    styleUrls:['live-channel.component.scss']
})

export class LiveChannelComponent implements OnInit, OnDestroy{
    
    @Input() channel: Channel;
    
    public tvshow: Tvshow;
    public progress: number;
    private timer: any;
    private readonly interval = 2000;

    constructor( private dataService: DataService){}
    ngOnInit(){
        this.getCurrentTvshow();
        this.startTimer();

    }

    public get isHaveTvshow(): boolean{
        if (this.tvshow){
            return true;
        } else {
            return false;
        }
    }
    private getCurrentTvshow(): void{
    this.dataService.getCurrentTvshow(this.channel.channel_id).then(res=>{
        this.tvshow=res;
        this.calculateProgress();
    });
}

private startTimer (): void{
    this.timer=setInterval(() =>{
     if(this.progress>1){
          this.progress =0;
          this.getCurrentTvshow();
     }
    }, this.interval);

}

private calculateProgress():void{
    if (this.tvshow){
        this.progress=(moment().unix()-this.tvshow.start)/(this.tvshow.stop-this.tvshow.start)
    }else {
        this.progress=0;
    }
}
ngOnDestroy(){
    clearInterval(this.timer);
}
}