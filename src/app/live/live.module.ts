import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { LivePage } from './live.page';
import { LiveChannelComponent } from '../components/live-channel/live-channel.component';
import { PipeModule } from '../Pipes/pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipeModule,
    
    RouterModule.forChild([
      {
        path: '',
        component: LivePage
      }
    ])
  ],
  declarations: [LivePage,
    LiveChannelComponent]
})
export class LivePageModule {} 