import { Component, OnInit } from "@angular/core";
import { Channel } from '../models/data';
import { DataService } from '../services/data.service';
import { LoadingController } from '@ionic/angular';


@Component({
    selector: 'live',
    templateUrl: 'live.page.html',
    styleUrls: ['live.page.scss']
})
export class LivePage implements OnInit{

    public channels: Channel[]=[];
    private loader: HTMLIonLoadingElement;

    constructor(private dataService: DataService, private loadingController: LoadingController){}

    ngOnInit(){
       this.startLoadIndicate();
        this.dataService.getChannels().then(res =>{
            this.channels = res;
            this.stopLoadIndicate();
        });
    }

    public get isHaveData(): boolean{
        return this.channels.length > 0;
    }

        private async startLoadIndicate(){
            this.loader = await this.loadingController.create({
              message: 'загрузка передач'
            });
            this.loader.present();
          }
        
          private stopLoadIndicate(){
            setTimeout(() =>{
              this.loader.dismiss();
            },4000);
          }
    }


